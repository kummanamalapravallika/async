import aiohttp
import asyncio
from bs4 import BeautifulSoup
from aiofile import async_open
import threading
from time import perf_counter
from concurrent.futures import ThreadPoolExecutor, as_completed


async def get_image_urls(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            html = await response.text()

    soup = BeautifulSoup(html, 'html.parser')
    image_urls = []
    for img in soup.find_all("img"):
        try:
            if img["class"][0].startswith("wp-image-"):
                img_url = img["src"].split("?")[0]
                image_urls.append(img_url)
        except KeyError as exc:
            pass
    return image_urls


async def download_image(url, filename):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            async with async_open(filename, 'wb+') as afp:
                async for chunk in response.content.iter_chunked(16*1024):
                    await afp.write(chunk)
    thread = str(threading.current_thread())
    print(f"{filename} downloaded successfully from {url} on {thread}")


async def async_download(urls):
    tasks = []
    for url in urls:
        task = asyncio.create_task(download_image(url, url.split("/")[-1]))
        tasks.append(task)

    await asyncio.gather(*tasks)


def main():
    image_urls = asyncio.run(get_image_urls(
        "https://triponzy.com/blog/most-beautiful-tourist-places-in-india/"))

    max_workers = 6
    for i in range(0, len(image_urls), max_workers):
        with ThreadPoolExecutor(max_workers=max_workers) as executor:
            executor.submit(asyncio.run, async_download(image_urls[i:i+max_workers]))


async def amain():
    image_urls = await get_image_urls(
        "https://triponzy.com/blog/most-beautiful-tourist-places-in-india/")
    max_workers = 5
    for i in range(0, len(image_urls), max_workers):
        urls = image_urls[i:i+max_workers]
        await async_download(urls)


if __name__ == "__main__":
    start_time = perf_counter()
    #main()
    asyncio.run(amain())
    end_time = perf_counter()
    time_lapsed = (end_time - start_time)
    print(f"finished executing in {time_lapsed}")
